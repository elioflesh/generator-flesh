;(function ($) {
  var app = $.sammy("main", function () {
    this.use("Template")

    this.get("#/", function (context) {
      this.load("http://localhost:3030/engage/<%= schemaName %>", {
        json: true,
      }).then(function (items) {
        $.each(items, function (i, item) {
          context
            .render("flesh/<%= name %>.template", {
              item: item,
            })
            .appendTo(context.$element())
        })
      })
    })

    $(function () {
      app.run("#/")
    })
  })
})(jQuery)
