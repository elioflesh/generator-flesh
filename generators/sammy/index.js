"use strict"
const Thingrator = require("generator-thing/generators/thingrator")
const sinsay = require("sinsay")
const mkdir = require("mkdirp")
const request = require("request")
require("fs")

// Gratuitous class extends so I know how to do this in the future.
class Flesh extends Generator {
  // Pretty pointless method just so I know how to do this in the future.
  stage(mess) {
    this.log(sinsay(mess))
  }
}

module.exports = class extends Flesh {
  initializing() {
    this.composeWith("sin:app", {
      options: {
        name: this.appname,
        h5bp: false,
        flesh: true,
      },
    })
  } // End initializing

  prompting() {
    const prompts = [
      {
        type: "input",
        name: "schemaName",
        message: "Which schema do you want to use?",
        default: "Thing",
      },
    ]

    return this.prompt(prompts).then((answer) => {
      // `this` will need these later.
      this.answer = answer
      // Store these so that we can use them as defaults.
      this.config.set("schemaName", answer.schemaName)
    })
  } // End prompting

  writing() {
    // mkdir("flesh")
    // mkdir("js")
    this.paths()
    const pkjJson = {
      main: "stylesheets/judge.scss",
      style: "dist/css/<%= name %>.min.css",
      dependencies: {
        "@elioway/god": "latest",
        "@elioway/adon": "latest",
        "@elioway/eve": "latest",
        "@elioway/bones": "latest",
        nodemon: "^1.18.3",
        sammy: "^0.7.6",
      },
      scripts: {
        build: "gulp",
        test: "mocha test/index.js",
      },
    }

    this.fs.copy(
      this.templatePath("flesh/readT.template"),
      this.destinationPath("flesh/" + this.appname + ".template")
    )
    var properName =
      this.appname.slice(0, 1).toUpperCase() +
      this.appname.slice(1).toLowerCase()
    this.fs.copy(
      this.templatePath("flesh/updateT.template"),
      this.destinationPath("flesh/" + properName + ".template")
    )
    this._copyFiles("elio-flesh-logo.png")
    this._renderFiles("js/app.js", "js/" + this.appname + ".js")
  } // End writing

  end() {
    this._copyFiles("apple-touch-icon.png")
    this._copyFiles("favicon.ico")
    this._copyFiles("favicon.ico")
    this._copyFiles("favicon.svg")
    this._copyFiles("tile-wide.png")
    this._copyFiles("tile.png")
    this._renderFiles("gulpfile.js", "gulpfile.js")
    this._renderFiles("index.html", "index.html")
    this._renderFiles("README.md", "README.md")
  }

  // HELPER: General use templater for all kinds of templates.
  _renderFiles(fileName, destName) {
    this.fs.copyTpl(
      this.templatePath(fileName),
      this.destinationPath(destName),
      {
        date: new Date().toISOString().split("T")[0],
        schemaName: this.answer.schemaName,
        name: this.appname,
      }
    )
  }

  // HELPER: Straightforward copy with no wildcards.
  _copyFiles(fileName) {
    this.fs.copy(this.templatePath(fileName), this.destinationPath(fileName))
  }
} // End InSinerator
