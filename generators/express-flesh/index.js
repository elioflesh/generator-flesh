"use strict"
const Thingrator = require("generator-thing/generators/thingrator")

module.exports = class Reporator extends Thingrator {
  initializing() {
    if (!this.options.composedBy) {
      this._eliosay("express-flesh")
      this._blackquote(
        `Thus they, in lowliest plight, repentant stood \n` +
          `Praying; for from the mercy-seat above \n` +
          `Prevenient grace descending had removed \n` +
          `The stony from their hearts, `
      )
    }
  }
  end() {
    if (!this.options.composedBy) {
      this._blackquote(
        `                                      and made new flesh`
      )
    }
  }
  writing() {
    this.paths()
    const kebabName = this.kebabName
    const ProperName = this.ProperName
    const camelName = this.camelName
    // package update
    const pkgJson = {
      bin: "./bin/index.js",
      dependencies: {
        "@elioway/bones": "^2.1.14",
        "@elioway/dbhell": "^1.0.0",
        "@elioway/express-flesh": "^0.1.11",
        "@elioway/exifyt": "^0.0.5",
        express: "^4.18.2",
        hbs: "^4.2.0",
        yargs: "^17.6.2",
      },
      devDependencies: {
        "@elioway/innocent": "^1.2.4",
        nodemon: "^2.0.20",
        prettier: "^2.8.4",
        reload: "^3.2.1",
        jquery: "^3.6.1",
        prettier: "2.8.0",
        "sanitize.css": "latest",
      },
      scripts: {
        [`${kebabName}`]: "node bin/index.js",
        build: "gulp build",
        dev: "nodemon server-reload.js --watch serve.js --watch server-reload.js --watch .data --watch stylesheets -e js,json,handlebars,scss",
        pm2: `pm2 start serve.js --namespace tew --name ${kebabName}`,
        ["prettier:hbs"]:
          'prettier  --write "**/*.{hbs,handlebars}" --parser=glimmer',
        prettier:
          'prettier --write "**/*.{css,html,js,json,md,mjs,scss,ts,yaml}" "!package.json"',
        serve: "node serve.js",
        test: "mocha --recursive",
      },
      type: "module",
    }
    // this._write("package.json") replaced
    this.fs.extendJSON(this.destinationPath("package.json"), pkgJson)
    // file system
    this._copy("bin/index.js")
    this._copy("media/2011-01-18-pet-parrot-koh-samui-thailand-9155501391.JPG")
    this._copy(
      "media/2011-01-20-gecko-theatre-lamai-koh-samui-thailand-9155299685.JPG"
    )
    this._copy("media/2011-02-02-will-the-last-person-to-leave-9157644222.JPG")
    this._copy("media/2011-02-14-sound-crew-9157760690.JPG")
    this._copy(
      "media/2011-05-02-the-boat-that-was-afraid-of-the-rain-9158743350.JPG"
    )
    this._copy("media/2011-05-16-protection-9156598937.JPG")
    this._copy("media/2011-05-21-little-tree-on-tab-kaek-beach-9157780212.JPG")
    this._write("doc/deploy.md")
    this._write("createServer.js")
    this._write("README.md")
    this._write("serve.js")
    this._write("server-reload.js")
    this._write("endpoints/camelNameT.js", `endpoints/${camelName}T.js`)
    this._write("views/404.handlebars")
    this._write("views/camelNameT.handlebars", `views/${camelName}T.handlebars`)
    this._write(
      "views/layouts/kebabName.handlebars",
      `views/layouts/${kebabName}.handlebars`
    )
    this._write(
      "views/partials/ProperName.handlebars",
      `views/partials/${ProperName}.handlebars`
    )
  }
}
