# Installing generator-flesh

- [generator-flesh Prerequisites](/elioflesh/generator-flesh/prerequisites.html)

```bash
npm install -g yo
npm install -g generator-flesh
```

## Updating

```
ncu -u -t minor
```
