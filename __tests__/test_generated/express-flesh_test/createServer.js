import * as path from "path"
import { fileURLToPath } from "url"
import db from "@elioway/dbhell"
import elioMadeFlesh from "@elioway/express-flesh"
import { adons, boneEnvVarsLoader, ribs, spine } from "@elioway/bones"
import expressFleshTestT from "./endpoints/expressFleshTestT.js"

export const createServer = (config) => {
  let __dirname = path.dirname(fileURLToPath(import.meta.url))
  let CFG = {
    ...boneEnvVarsLoader(true, "", process.cwd()),
    PORT: 3000,
    HBS: {
      VIEWSFOLDER: path.resolve(__dirname, "views"),
      defaultLayout: "express-flesh-test",
      helpers: {
        digMe: (context) => `We like ${context}`,
      },
    },
    ...config,
  }
  return elioMadeFlesh(CFG, { ...ribs, ...spine, ...adons }, db).then((app) => {
    // Overide templating and/or add some more partials
    let hbs = app.get("hbs")
    // change defaultLayout or see above 
    // hbs.defaultLayout = "express-flesh-test"
    // add another helper or see above.
    hbs.helpers.cutMe = (context) => context.slice(0, 3)
    // add another partial
    // https://github.com/express-handlebars/express-handlebars/blob/master/examples/advanced/server.js
    hbs.handlebars.registerPartial("Debug", "{{cutMe identifier}}")
    // Use a different endpoint.
    console.dir(app)
    app.get("/", expressFleshTestT)
    // Pass the app out into the promise.
    return app
  })
}

export default createServer
