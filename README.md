![](https://elioway.gitlab.io/elioflesh/elio-generator-flesh-flesh-logo.png)

# generator-flesh ![notstarted](/devops/notstarted/favicon.ico "notstarted")

yeoman generator for a SPA working with the bones REST API **the elioWay**.

## Installation

First, install [Yeoman](http://yeoman.io) and generator-flesh using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g gulp-cli qunit mocha yo generator-flesh
```

Then clone the elio **bones** project:

```bash
git clone https://gitlab.com/elioflesh/bones.git myapp
cd myapp
yo sin
yo flesh
```

## Getting To Know Yeoman

- Yeoman has a heart of gold.
- Yeoman is a person with feelings and opinions, but is very easy to work with.
- Yeoman can be too opinionated at times but is easily convinced not to be.
- Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT [Tim Bushell](mailto:theElioWay@gmail.com)

[elioWay](https://gitlab.com/elioway/elio/blob/master/README.md)

![](apple-touch-icon.png)

[daviddm-image]: https://david-dm.org/timitee/generator-flesh.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/timitee/generator-flesh
[npm-image]: https://badge.fury.io/js/generator-flesh.svg
[npm-url]: https://npmjs.org/package/generator-flesh
[travis-image]: https://travis-ci.org/timitee/generator-flesh.svg?branch=master
[travis-url]: https://travis-ci.org/timitee/generator-flesh
